import React from 'react';
import "./CharacterCard.css"

const CharacterCard = ({character}) => {
    let lol = character.design
    return (
        <details className={"maindetails"}>
            <summary>
                <span>{character.name}</span>
            </summary>
            {lol ? <img src={character.design} alt={"x"}/> : <span></span>}

            <details className={"subdetails"}>
                <summary className={"subsummary"}>Биография и Характер</summary>
                <br/>
                Биография: <br/>{character.bio} <br/> <br/>
                Характер: <br/>{character.personality} <br/> <br/>
                Фракция: <br/> {character.fraction} <br/> <br/>
                Ранг: <br/> {character.rank} <br/><br/>
                Рост: <br/>{character.height} <br/> <br/>
                Вес: <br/>{character.weight} <br/> <br/>
                <br/>
            </details>

            <details className={"subdetails"}>
                <summary className={"subsummary"}>Способности и Характеристики</summary>
                <br/>
                Характеристики: <br/> {character.characteristics} <br/> <br/>
                Дьявольский Фрукт: <br/> {character["devil fruits"]} <br/> <br/>
                Хаки: <br/> {character.haki} <br/> <br/>
                Боевой Стиль: <br/> {character.style} <br/> <br/>
                Корабли: <br/> {character.ships} <br/> <br/>
                Белли: <br/> {character.belly} белли <br/> <br/>
                Награда за голову: <br/> {character.reward} белли <br/> <br/>
            </details>
        </details>
    );
};

export default CharacterCard;