import React from 'react';
import CharacterCard from "../CharacterCard/CharacterCard";
import characterCard from "../CharacterCard/CharacterCard";

const CharactersList = ({characters}) => {

    const charactersCard = characters.map((character, i) => <CharacterCard key={i} character={character}/>)
    return (
        <div className={"CharactersList-wrapper"}>
            {charactersCard}
        </div>
    );
};

export default CharactersList;