import './App.css';
import CharactersList from "./components/CharactersList/CharactersList";
import {useEffect, useState} from "react";
import data from "./characters.json";


function App() {
    const [characters, setCharacters] = useState(data)
    const [input, setInput] = useState("")

    useEffect(() => {
        let summaries = document.querySelectorAll("summary")
        for(let i = 0; i < summaries.length; i++) {
            if(summaries[i].innerText.includes(input) === false) {
                summaries[i].parentElement.classList.add("hide")
            } else {
                summaries[i].parentElement.classList.remove("hide")
            }
        }
    }, [input])

  return (
    <div className="App">
        <div className={"input-wrapper"}>
            <input type={"text"} placeholder={"Введите имя персонажа для быстрого поиска"} onChange={e => setInput(e.target.value)}/> <br/>
        </div>
            <CharactersList characters={characters}/>
    </div>
  );
}

export default App;